Rails.application.routes.draw do

  devise_for :users
  resources :homes, only:  [:index]
  resources :posts do
    resources :comments
    resources :favourites, only: [:create, :destroy]
  end

  resources :favourites, only: [:index]

  get "/abouts" => "abouts#index"


  root "homes#index"

end