class Post < ActiveRecord::Base
  validates :title, presence: true,
                    uniqueness: {case_sensitive: false}

  has_many :comments, dependent: :destroy
  has_many :favourites, dependent: :destroy
  has_many :favourited_users, through: :favourites, source: :user
  has_many :categorizations, dependent: :destroy
  has_many :categories, through: :categorizations

  belongs_to :user

  def favourite_for(user)
    favourites.find_by_user_id(user.id) if user
  end

end
