class Category < ActiveRecord::Base
  validates :title, presence: true

  has_many :categorizations, dependent: :destroy
  has_many :posts, through: :categorizations
end
