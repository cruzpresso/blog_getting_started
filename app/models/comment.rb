class Comment < ActiveRecord::Base
  validates :body, presence: true,
                   uniqueness: {scope: :post_id}

  belongs_to :post
  belongs_to :user

  scope :recent, lambda { order("created_at DESC") }

end
