class CommentsController < ApplicationController
  before_action :authenticate_user!
  
  def create
    @post = Post.find params[:post_id]
    @comment = Comment.new(comment_params)
    @comment.user = current_user
    @comment.post = @post

    if @comment.save
      redirect_to post_path(@post), notice: "Comment created"
    else
      render "posts/show"
    end
  end

  def edit
    @post = Post.find params[:post_id]
    @comment   = Comment.find params[:id]
  end

  def update
    @post = Post.find params[:post_id]
    @comment   = Comment.find params[:id]
    if @comment.update comment_params
      redirect_to @post, notice: "Comment updated"
    else
      render :edit
    end
  end

  def destroy
    @post = Post.find params[:post_id]
    @comment = Comment.find params[:id]
    @comment.destroy
    redirect_to post_path(@post), notice: "Comment deleted"
  end

  private

  def comment_params
    params.require(:comment).permit(:body)
  end
end
