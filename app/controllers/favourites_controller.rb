class FavouritesController < ApplicationController
  before_action :authenticate_user!

  def index
    @favourited_posts = current_user.favourited_posts
  end

  def create
    @post = Post.find params[:post_id]
    @favourite = current_user.favourites.new
    @favourite.post = @post
    if @favourite.save
      redirect_to @post, notice: "Favourited!"
    else
      redirect_to @post, alert: "Not favourited!"
    end
  end

  def destroy
    @favourite = current_user.favourites.find params[:id]
    @post = @favourite.post
    @favourite.destroy
    redirect_to @post, notice: "Unfavourited"
  end

end