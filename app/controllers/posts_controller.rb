class PostsController < ApplicationController
  before_action :authenticate_user!, except: [:index, :show]
  before_action :find_post, only: [:edit, :update, :destroy]

  def index
    @posts = Post.all
  end

  def new
    @post = Post.new
  end

  def create
    @post = current_user.posts.new(post_params)
    if @post.save
      flash[:notice] = "Blog post saved"
      redirect_to post_path(@post)
    else
      flash[:notice] = "You can't have empty fields"
      render :new
    end
  end

  def show
    @post = Post.find params[:id]
    @comment = Comment.new
    @favourite = @post.favourite_for(current_user) if user_signed_in?
  end

  def edit
  end

  def update
    if @post.update(post_params)
      flash[:notice] = "Blog post updated"
      redirect_to @post
    else
      flash[:notice] = "You can't have empty fields"
      render :edit
    end
  end

  def destroy
    @post.destroy
    redirect_to posts_path
  end

  private

  def find_post
    @post = Post.find(params[:id])
  end

  def post_params
    params.require(:post).permit(:title, :body, {category_ids: []})
  end

end
